package demo.annotation;

/**
 * 〈〉
 *
 * @author Noah2021
 * @create 2021/3/6
 * @return
 */
@CourseInfoAnnotation(courseName = "数学",courseTag = "高中",courseProfile = "又难又多")
public class Noah2021Course {
    @PersonInfoAnnotation(name = "Noah2021",language = {"Java","c++","python"})
    private String author;
    @CourseInfoAnnotation(courseName = "化学",courseTag = "理综",courseProfile = "非常难")
    public void getCourseInfo(){
    }
}