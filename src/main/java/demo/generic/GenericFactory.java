package demo.generic;

public interface GenericFactory<T,N> {
    T nextObject();
    N nextNumber();
}
