package demo.generic;

import lombok.Data;

/**
 * 〈〉
 *
 * @author Noah2021
 * @create 2021/3/6
 * @return
 */
@Data
public class GenericClassExample<T> {
    private T member;

    public GenericClassExample(T member) {
        this.member = member;
    }

    public T headSomething(T target) {
        return target;
    }

    public String sayHello(String name) {
        return "hello, " + name;
    }

    public static <T> void printArray(T[] inputArray) {
        for (T element : inputArray) {
            System.out.printf("%s", element);
            System.out.print(" ");
        }
        System.out.println();
    }
}