package demo.generic;

/**
 * 〈〉
 *
 * @author Noah2021
 * @create 2021/3/6
 * @return
 */
public class GeneraicDemo {
    public static void handMember(GenericClassExample<? super Integer> genericClassExample){
        Integer integer = 123 + (Integer) genericClassExample.getMember();
        System.out.println("result is " + integer);
    }

    public static void main(String[] args) {
        GenericClassExample<Number> integerExample = new GenericClassExample<Number>(123);
        GenericClassExample<String> stringExample = new GenericClassExample<>("abc");
        System.out.println(integerExample.getClass());
        System.out.println(stringExample.getClass());
        handMember(integerExample);
        Integer[] integers = {1, 2, 3, 4, 5, 6};
        Double[] doubles = {1.1, 1.2, 1.3, 1.4, 1.5};
        Character[] characters = {'A', 'B', 'C'};
        stringExample.printArray(integers);
        stringExample.printArray(doubles);
        stringExample.printArray(characters);
    }
    /*  class demo.generic.GenericClassExample   //说明泛型只存在与编译期，详情请移步“泛型擦除”
        class demo.generic.GenericClassExample   //目的是避免过多的创建类而造成的运行时的过度消耗
        result is 246
        1 2 3 4 5 6
        1.1 1.2 1.3 1.4 1.5
        A B C
    * */
}