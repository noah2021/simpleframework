package demo.pattern.factory.method;

import demo.pattern.factory.entity.LenovoMouse;
import demo.pattern.factory.entity.Mouse;

/**
 * 〈〉
 *
 * @author Noah2021
 * @create 2021/3/6
 * @return
 */
public class LenovoMouseFactory implements MouseFactory {

    @Override
    public Mouse createMouse() {
        return new LenovoMouse();
    }
}