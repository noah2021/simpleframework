package demo.pattern.factory.entity;
/**
 * 〈〉
 *
 * @author Noah2021
 * @create 2021/3/6
 * @return
 */
public interface Mouse {
    void sayHi();
}
