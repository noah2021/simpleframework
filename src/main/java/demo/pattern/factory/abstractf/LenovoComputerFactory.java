package demo.pattern.factory.abstractf;

import demo.pattern.factory.entity.*;

/**
 * 〈〉
 *
 * @author Noah2021
 * @create 2021/3/6
 * @return
 */
public class LenovoComputerFactory implements ComputerFactory {

    @Override
    public Mouse createMouse() {
        return new LenovoMouse();
    }

    @Override
    public KeyBoard createKeyBoard() {
        return new LenovoKeyBoard();
    }
}