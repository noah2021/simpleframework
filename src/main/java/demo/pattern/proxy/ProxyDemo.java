package demo.pattern.proxy;

import demo.pattern.proxy.cglib.AliPayMethodInterceptor;
import demo.pattern.proxy.cglib.CglibDanamicProxyUtil;
import demo.pattern.proxy.impl.AlipayToC;
import demo.pattern.proxy.impl.CommonPayment;
import demo.pattern.proxy.impl.ToCPaymentImpl;
import demo.pattern.proxy.jdkproxy.AlipayInvocationHandler;
import demo.pattern.proxy.jdkproxy.JdkDynamicProxyUtil;

/**
 * 〈〉
 *
 * @author Noah2021
 * @create 2021/3/9
 * @return
 */
//只需要写一个实现接口的被代理类，省略了之前AlipayToC类的具体实现，提高代码复用
public class ProxyDemo {
    public static void main(String[] args) {
        /*ToCPayment toCProxy = new AlipayToC(new ToCPaymentImpl());
        toCProxy.pay();*/
        ToCPaymentImpl toCPayment = new ToCPaymentImpl();
        /*AlipayInvocationHandler h = new AlipayInvocationHandler(toCPayment);
        ToCPayment toCProxy = JdkDynamicProxyUtil.newProxyInstance(toCPayment, h);
        toCProxy.pay();*/
        //CommonPayment commonPayment = new CommonPayment();
        AliPayMethodInterceptor interceptor = new AliPayMethodInterceptor();
        ToCPaymentImpl toCProxy = CglibDanamicProxyUtil.createProxy(toCPayment, interceptor);
        toCProxy.pay();
    }
}