package demo.pattern.proxy.impl;

import demo.pattern.proxy.ToCPayment;

/**
 * 〈〉
 *
 * @author Noah2021
 * @create 2021/3/9
 * @return
 */
public class AlipayToC implements ToCPayment {
    private ToCPayment toCPayment;
    public AlipayToC(ToCPayment toCPayment) {
        this.toCPayment = toCPayment;
    }
    @Override
    public void pay() {
        Before();
        toCPayment.pay();
        After();
    }
    private void After() {
        System.out.println("付钱给慕课网");
    }
    private void Before() {
        System.out.println("从招行取款");
    }
}