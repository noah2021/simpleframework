package demo.pattern.proxy.impl;
/**
 * 〈〉
 *
 * @author Noah2021
 * @create 2021/3/9
 * @return
 */
public class CommonPayment {
    public void pay() {
        System.out.println("个人名义或者公司名义都可以走这个支付通道");
    }
}
