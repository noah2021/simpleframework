package demo.pattern.proxy.cglib;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;

/**
 * 〈〉
 *
 * @author Noah2021
 * @create 2021/3/9
 * @return
 */
public class CglibDanamicProxyUtil {
    public static <T>T createProxy(T target, MethodInterceptor methodInterceptor){
        return (T)Enhancer.create(target.getClass(),methodInterceptor);
    }
}