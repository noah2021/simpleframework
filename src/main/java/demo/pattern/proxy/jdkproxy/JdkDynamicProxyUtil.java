package demo.pattern.proxy.jdkproxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * 〈〉
 *
 * @author Noah2021
 * @create 2021/3/9
 * @return
 */
public class JdkDynamicProxyUtil {
    public static <T>T newProxyInstance(Object target, InvocationHandler h){
        ClassLoader classLoader = target.getClass().getClassLoader();
        Class<T>[] interfaces = (Class<T>[]) target.getClass().getInterfaces();
        return (T)Proxy.newProxyInstance(classLoader,interfaces,h);
    }
}