package demo.pattern.proxy.jdkproxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 〈〉
 *
 * @author Noah2021
 * @create 2021/3/9
 * @return
 */
public class AlipayInvocationHandler implements InvocationHandler {
    private Object target;

    public AlipayInvocationHandler(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Before();
        Object result = method.invoke(target, args);
        After();
        return result;
    }
    private void After() {
        System.out.println("付钱给慕课网");
    }
    private void Before() {
        System.out.println("从招行取款");
    }
}