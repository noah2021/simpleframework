package demo.pattern.observer;

public interface EventListener {
    void processEvent(Event event);
}
