package org.simpleframework.aop.aspect;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.simpleframework.aop.PointcutLocator;

/**
 * 〈〉
 *
 * @author Noah2021
 * @create 2021/3/11
 * @return
 */
@AllArgsConstructor
@Data
public class AspectInfo {
    private int orderIndex;
    private DefaultAspect aspectObject;
    private PointcutLocator pointcutLocator;
}