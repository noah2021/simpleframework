package org.simpleframework.mvc.render;

import org.simpleframework.mvc.RequestProcessorChain;

/**
 * 渲染处理请求
 */
public interface ResultRender {
    //执行渲染
    public void render(RequestProcessorChain requestProcessorChain) throws Exception;
}
