package com.noah2021.entity.dto;

import com.noah2021.entity.bo.HeadLine;
import com.noah2021.entity.bo.ShopCategory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 〈〉
 *
 * @author Noah2021
 * @create 2021/3/6
 * @return
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MainPageInfoDTO {
    private List<HeadLine> headLineList;
    private List<ShopCategory> shopCategoryList;
}