package com.noah2021.controller.frontend;

import com.noah2021.entity.dto.MainPageInfoDTO;
import com.noah2021.entity.dto.Result;
import com.noah2021.service.combine.HeadLineShopCategoryCombineService;
import com.noah2021.service.combine.impl.HeadLineShopCategoryCombineServiceImpl1;
import lombok.Getter;
import org.simpleframework.core.annotation.Controller;
import org.simpleframework.inject.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 〈〉
 *
 * @author Noah2021
 * @create 2021/3/6
 * @return
 */
@Getter
@Controller
public class MainPageController {
    @Autowired(value = "HeadLineShopCategoryCombineServiceImpl1")
    private HeadLineShopCategoryCombineService headLineShopCategoryCombineService;
    Result<MainPageInfoDTO> getMainPageInfo(HttpServletRequest req, HttpServletResponse resp){
        return headLineShopCategoryCombineService.getMainPageInfo();
    }

}