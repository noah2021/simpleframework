package com.noah2021.aspect;

import org.simpleframework.aop.annotation.Aspect;
import org.simpleframework.aop.annotation.Order;
import org.simpleframework.aop.aspect.DefaultAspect;
import org.simpleframework.core.annotation.Controller;

import java.lang.reflect.Method;

/**
 * 〈〉
 *
 * @author Noah2021
 * @create 2021/3/12
 * @return
 */

@Aspect(pointcut = "execution(* com.noah2021.controller.superadmin..*.*(..))")
@Order(0)
public class ControllerTimeCalculatorAspect extends DefaultAspect {
    private long timeStampCache;
    @Override
    public void before(Class<?> target, Method method, Object[] args) throws Throwable {
        System.out.println("【INFO】开始计时，执行的类是"+target.getName()+"，执行的方法是："+method.getName()+"，参数是："+args);
        timeStampCache = System.currentTimeMillis();
    }

    @Override
    public Object afterReturning(Class<?> target, Method method, Object[] args, Object returnValue) throws Throwable {
        long endTime = System.currentTimeMillis();
        long costTime = endTime - timeStampCache;
        System.out.println("【INFO】结束计时，执行的类是"+target.getName()+"，执行的方法是："+method.getName()+"，参数是："+args+
                "，返回值是"+returnValue+"，时间是："+costTime);
        return returnValue;

    }
}