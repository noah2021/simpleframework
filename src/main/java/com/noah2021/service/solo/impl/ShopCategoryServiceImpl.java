package com.noah2021.service.solo.impl;

import com.noah2021.entity.bo.ShopCategory;
import com.noah2021.entity.dto.Result;
import com.noah2021.service.solo.ShopCategoryService;
import org.simpleframework.core.annotation.Service;

import java.util.List;
/**
 * 〈〉
 *
 * @author Noah2021
 * @create 2021/3/6
 * @return
 */
@Service
public class ShopCategoryServiceImpl implements ShopCategoryService {
    @Override
    public Result<Boolean> addShopCategory(ShopCategory shopCategory) {
        return null;
    }

    @Override
    public Result<Boolean> removeShopCategory(int shopCategoryId) {
        return null;
    }

    @Override
    public Result<Boolean> modifyShopCategory(ShopCategory shopCategory) {
        return null;
    }

    @Override
    public Result<ShopCategory> queryShopCategoryById(int shopCategoryId) {
        return null;
    }

    @Override
    public Result<List<ShopCategory>> queryShopCategory(ShopCategory shopCategoryCondition, int pageIndex, int pageSize) {
        return null;
    }
}
