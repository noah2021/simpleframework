package com.noah2021.service.combine;


import com.noah2021.entity.dto.MainPageInfoDTO;
import com.noah2021.entity.dto.Result;

/**
 * 〈〉
 *
 * @author Noah2021
 * @create 2021/3/6
 * @return
 */
public interface HeadLineShopCategoryCombineService {
    Result<MainPageInfoDTO> getMainPageInfo();
}
