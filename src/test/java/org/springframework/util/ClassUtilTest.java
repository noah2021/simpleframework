package org.springframework.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.simpleframework.util.ClassUtil;

import java.util.Set;

/**
 * 〈〉
 *
 * @author Noah2021
 * @create 2021/3/7
 * @return
 */
public class ClassUtilTest {
    @DisplayName("提取目标类方法:extractPackageClassTest")
    @Test
    public void extractPackageClassTest() {
        Set<Class<?>> classSet = ClassUtil.extractPackageClass("com.noah2021.entity");
        for (Class clazz : classSet) {
            System.out.println(clazz);
        }
        Assertions.assertEquals(4, classSet.size());
    }
}