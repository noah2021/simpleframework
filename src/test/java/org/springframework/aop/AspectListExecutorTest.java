package org.springframework.aop;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.simpleframework.aop.AspectListExecutor;
import org.simpleframework.aop.aspect.AspectInfo;
import org.springframework.aop.mock.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 〈〉
 *
 * @author Noah2021
 * @create 2021/3/11
 * @return
 */
public class AspectListExecutorTest {
    @DisplayName("Aspect排序")
    @Test
    public void sortTest(){
        ArrayList<AspectInfo> aspectInfoList = new ArrayList<>();
        aspectInfoList.add(new AspectInfo(4, new Mock1(),null));
        aspectInfoList.add(new AspectInfo(5, new Mock4(),null));
        aspectInfoList.add(new AspectInfo(2, new Mock3(),null));
        aspectInfoList.add(new AspectInfo(3, new Mock5(),null));
        aspectInfoList.add(new AspectInfo(1, new Mock2(),null));
        List<AspectInfo> sortAspectInfoList = new AspectListExecutor(AspectListExecutorTest.class, aspectInfoList).getSortedAspectInfoList();
        for (AspectInfo info: sortAspectInfoList) {
            System.out.println(info);
        }
    }
}